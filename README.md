# 2017_dmp_external_sdk

The dmp library for stroing data.

## Installation

```
  npm install https://bitbucket.org/freshdigitalgroup/2017_dmp_external_sdk.git --save
```

## Usage
To write a skill using SDK

  * Register for a developer account and get your client_id and secret at https://beyondvoice.freshdigitalgroup.com
  * Add dependency in your package.json file.
  ```
    "dmp_external_sdk": "git+ssh://git@bitbucket.org:freshdigitalgroup/2017_dmp_external_sdk.git"
  ```
  * Add metadata information (change 'AMZN' to 'MSFT' for Cortana and to 'GOOG' for Google).
  ```
    const metadata = {
      'account_id': '',
      'platform': 'AMZN',
    };
  ```
  * Add configuration information.
  ```
    const config = {
      'mode': 'live' || sandbox',
      'client_id': '',
      'client_secret': ''
    };
  ```
  * Require the library.
  ```
    const DMP = require('dmp_external_sdk')(metadata, config);
  ```
  * AMAZON ALEXA: Pass in the export handler function to DMP.
  ```
    exports.handler = function(event, context, callback) {
      var alexa = Alexa.handler(event, context, callback);
      alexa.registerHandlers(handlers);
    };
  ```
  Change the above to:
  ```
    exports.handler = DMP.alexa.handler(function(event, context, callback) {
      var alexa = Alexa.handler(event, context);
      alexa.registerHandlers(handlers);
    });
  ```
  * MICROSOFT CORTANA: Pass in the skill builder to DMP after defining the skill builder. For example:
  ```
  let skill = new builder.Universalskill(connector, function (session) {
    session.beginDialog('Microsoft.Launch');
  });
  skill.use(DMP.cortana);
  ```
  * FACEBOOK MESSENGER WEBHOOK: Pass in request body, response body, and callback function to the DMP when posting to the webhook.
  ```
  app.post(webHookPath, function(req, res) {
    const messagingEvents = req.body.entry[0].messaging;
    if (messagingEvents.length && (messagingEvents[0].message && messagingEvents[0].message.text || messagingEvents[0].postback && messagingEvents[0].postback.payload)) {
      const event = req.body.entry[0].messaging[0];
      const sender = event.sender.id;
      if (!pausedUsers[sender]) {
        const text = event.message?event.message.text:null;
        const payload = event.postback?event.postback.payload:null;
        const message = getMessage(text, payload);
        const json = {
          recipient: {id: sender}
        };
        if (message.sender_action) {
          json.sender_action = message.sender_action;
        } else {
          json.message = message;
        }
        const requestData = {
          url: 'https://graph.facebook.com/v2.6/me/messages',
          qs: {access_token: process.env.FACEBOOK_PAGE_TOKEN},
          method: 'POST',
          json: json
        };
        request(requestData, function(error, response, body) {
          if (!process.env.FACEBOOK_IS_ECHO) {
            dmp.facebook.message(req.body, response.body, callbackFunction);
          }
        });
      }
    }
    res.sendStatus(200);
  });
  ```
  * When creating a skill/skill on the DMP, add optional scopes to access information from the DMP. For multiple scopes, seperate each scope with a space, and add the default scope as well.
  ```
    default scope:
    public

    optional scopes:
    skill_read
    skill_create
    skill_update
    skill_destroy
    category_search
    category_read
    client_user_attribute_read
    client_user_attribute_create
    client_user_attribute_update
    client_user_attribute_delete
    client_user_read
    client_user_create
    client_user_update
    client_user_destroy
    post_search
    post_read
    role_read
  ```
  * When accessing the data from the DMP through the SDK, pass in the required parameters after requiring and configuring the DMP SDK.
  ```
  const DMP = require('dmp_internal_sdk')(metadata, config);

  // Getting a skill
  let skillId = skillId;
  let params = {
    'account_id': accountId
  };
  DMP.skill.get(skillId, params, cb);

  // Getting the list of all skills
  let params = {
    'account_id': accountId
  };
  DMP.skill.list(params, cb)

  // Creating a skill
  let params = {
    'account_id': accountId,
    // skill params
  };
  DMP.skill.create(params, cb);

  // Deleting a skill
  let params = {
    'account_id': accountId,
  };
  let skillId = skillId;
  DMP.skill.del(skillId, params, cb);

  // Updating a skill
  let skillId = skillId;
  let params = {
    account_id: accountId,
    // update params
  };
  DMP.skill.update(skillId, params, cb);
  ```
  * Available search functions for the CMS (posts and categories).
  ```
  // Searching posts with a title
  let params = {
    'title': postTitle,
    'account_id': accountId,
  };
  DMP.post.search(params, cb);
  ```
  * Please don't hard code account_id, client_id and client_secret in your production code. Store them on Jenkins, and let build file set them as the Environment Variable of Lambda function. Then your code could get them as environment variables.

## DMP Security
Please check 'DMP Security' session under 'Jenkins_ReadMe.md' file for more information.

## Release History

* 1.1.1 Add FB message handler.
* 1.0.78 Add CMS SDK for DMP.
* 1.0.1 Add setter and getter for skill name.
* 1.0.0 Initial release
