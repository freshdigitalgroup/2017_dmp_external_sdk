// Convenience file to require the SDK from the root of the repository
// module.exports = require('./lib/dmp.js')();

'use strict';

const Alexa = require('./lib/alexa_handler');
const Cortana = require('./lib/cortana');
const Google = require('./lib/google');
const Facebook = require('./lib/facebook');

const Post = require('./lib/resources/clients/Post');
const Category = require('./lib/resources/clients/Category');
const ClientUserAttribute = require('./lib/resources/clients/Client_user_attributes');
const Skill = require('./lib/resources/clients/Skills');
const ClientUser = require('./lib/resources/clients/Client_users');
const Role = require('./lib/resources/clients/Roles');

const client = require('./lib/client');

module.exports = function(messageMetadata, options) {
  return {
    version: client.sdkVersion,
    // configure: configure,
    configuration: client.default_options,
    // generateToken: generateToken,
    skill: new Skill(messageMetadata, options),
    clientUser: new ClientUser(messageMetadata, options),
    role: new Role(messageMetadata, options),
    clientUserAttribute: new ClientUserAttribute(messageMetadata, options),
    post: new Post(messageMetadata, options),
    category: new Category(messageMetadata, options),
    alexa: new Alexa(messageMetadata, options),
    cortana: new Cortana(messageMetadata, options),
    google: new Google(messageMetadata, options),
    facebook: new Facebook(messageMetadata, options),
  };
};
