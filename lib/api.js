/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const client = require('./client');

/**
 * tokenPersist client id to access token cache,
 * used to reduce access token round trips
 * @type {Object}
 */
let tokenPersist = {};

/**
 * Set up configuration globally such as client_id and client_secret,
 * by merging user provided configurations otherwise use default settings
 * @param  {Object} options Configuration parameters passed as object
 * @return {undefined}
 */
exports.configure = function configure(options) {
  if (options !== undefined && typeof options === 'object') {
    client.default_options = merge(client.default_options, options);
  }

  if (client.default_options.mode !== 'sandbox' && client.default_options.mode !== 'live') {
    throw new Error('Mode must be "sandbox" or "live"');
  }
};

/**
 * Generate new access token by making a POST request to /oauth2/token by
 * exchanging base64 encoded client id/secret pair or valid refresh token.
 *
 * Otherwise authorization code from a mobile device can be exchanged for a long
 * living refresh token used to charge user who has consented to
 * future payments.
 * @param  {Object|Function}   config Configuration parameters such as
 *                                    authorization code or refresh token
 * @param  {Function} cb     Callback function
 */
const generateToken =
  exports.generateToken = function generateToken(config, cb) {
    if (typeof config === 'function') {
      cb = config;
      config = client.default_options;
    } else if (!config) {
      config = client.default_options;
    } else {
      config = merge(config, client.default_options, true);
    }

    var payload = 'grant_type=client_credentials';
    if (config.authorization_code) {
      payload = 'grant_type=authorization_code&response_type=token&' +
        'redirect_uri=urn:ietf:wg:oauth:2.0:oob&code=' +
        config.authorization_code;
    } else if (config.refresh_token) {
      payload = 'grant_type=refresh_token&refresh_token=' + config.refresh_token;
    }

    const basicAuthString = 'Basic ' + new Buffer(config.client_id + ':' + config.client_secret).toString('base64');

    const httpOptions = {
      schema: config.schema || client.default_options.schema,
      host: config.host || getDefaultApiEndpoint(config.mode) || client.default_options.host,
      port: config.port || client.default_options.port,
      headers: merge({
        'Authorization': basicAuthString,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      }, client.default_options.headers, true),
    };

    client.invoke('POST', '/oauth/token', payload, httpOptions, function (err, res) {
      var token = null;
      if (res) {
        console.log(res);
        // Ask for auth code
        if (!config.authorization_code && !config.refresh_token) {
          const seconds = new Date().getTime() / 1000;
          tokenPersist[config.client_id] = res;
          tokenPersist[config.client_id].created_at = seconds;
        }

        token = res.token_type + ' ' + res.access_token;
      }
      cb(err, token);
    });
  };

/* Update authorization header with new token obtained by calling
generateToken */
/**
 * Updates http Authorization header to newly created access token
 * @param  {Object}   httpOptions   Configuration parameters such as
 * authorization code or refresh token
 * @param  {Function}   errorCallback
 * @param  {Function} callback
 */
function updateToken(httpOptions, errorCallback, callback) {
  // console.log('update token httpoptions', httpOptions);

  generateToken(httpOptions, function(error, token) {
    if (error) {
      console.log('update token Error!!', error);
      console.log('update token errorcallback', errorCallback);

      errorCallback(error, token);
    } else {
      httpOptions.headers.Authorization = token;
      callback();
    }
  });
}

/**
 * Makes a Fresh Digital API call. Reuses valid access tokens to reduce
 * round trips, handles 401 error and token expiration.
 * @param  {String}   httpMethod           A HTTP Verb e.g. GET or POST
 * @param  {String}   path                  Url endpoint for API request
 * @param  {Data}   data                    Payload associated with API request
 * @param  {Object|Function}   httpOptions Configurations for settings and Auth
 * @param  {Function} cb                    Callback function
 */
exports.executeHttp = function executeHttp(httpMethod, path, data, httpOptions, cb) {
  if (typeof httpOptions === 'function') {
    cb = httpOptions;
    httpOptions = null;
  }
  if (!httpOptions) {
    httpOptions = client.default_options;
  } else {
    httpOptions = merge(httpOptions,
      client.default_options, true);
  }

  // Get host endpoint using mode
  httpOptions.host = httpOptions.host || getDefaultApiEndpoint(httpOptions.mode);
  /**
   * Retry a invoke call
   */
  function retryInvoke() {
    client.invoke(httpMethod, path, data, httpOptions, cb);
  }

  // correlation-id is deprecated in favor of client-metadata-id

  // If client_id exists with an unexpired token and
  // a refresh token is not provided, reuse cached token

  if (httpOptions.client_id in tokenPersist &&
    !checkExpiredToken(tokenPersist[httpOptions.client_id]) && !httpOptions.refresh_token) {
    httpOptions.headers.Authorization = 'Bearer ' + tokenPersist[httpOptions.client_id].access_token;
    client.invoke(httpMethod, path, data, httpOptions, function (error, response) {
      // Don't reprompt already authenticated user for
      // login by updating Authorization header
      // if token expires
      if (error && error.httpStatusCode === 401 &&
        httpOptions.client_id && httpOptions.headers.Authorization) {
        httpOptions.headers.Authorization = null;
        updateToken(httpOptions, cb, retryInvoke);
      } else {
        cb(error, response);
      }
    });
  } else {
    updateToken(httpOptions, cb, retryInvoke);
  }
};

/**
 * Get the default endpoint depends on the mode.
 *
 * @param  {String} mode
 * @return {String}
 */
const getDefaultEndpoint =
  exports.getDefaultEndpoint = function getDefaultEndpoint(mode) {
    return (typeof mode === 'string' && mode === 'live') ?
      'beyondvoice.freshdigitalgroup.com' : 'sandbox.beyondvoice.freshdigitalgroup.com';
  };

/**
 * Return the default api endpoint depends on the mode.
 *
 * @param  {String} mode
 * @return {String}
 */
const getDefaultApiEndpoint =
  exports.getDefaultApiEndpoint = function getDefaultApiEndpoint(mode) {
    const api = (typeof mode === 'string' && mode === 'security-test-sandbox') ?
      'test-api.' : 'api.';
    return api + getDefaultEndpoint(mode);
  };

/**
 * Recursively copies given object into a new object. Helper method for merge
 * @param  {Object} v
 * @return {Object}
 */
function clone(v) {
  if (v === null || typeof v !== 'object') {
    return v;
  }
  if (Array.isArray(v)) {
    var arr = v.slice();
    for (let i = 0; i < v.length; i++) {
      arr[i] = clone(arr[i]);
    }
    return arr;
  } else {
    var obj = {};
    for (var k in v) {
      obj[k] = clone(v[k]);
    }
    return obj;
  }
}

/**
 * Merges two Objects recursively, setting property of obj1 to those of obj2
 * and creating property as necessary.
 *
 * @param  {Object} obj1
 * @param  {Object} obj2
 * @param  {Boolean} appendOnly
 * @return {Object}
 */
const merge =
  exports.merge = function merge(obj1, obj2, appendOnly) {
    // Handle invalid arguments
    if (obj1 === null || typeof obj1 !== 'object') {
      throw new TypeError(
        'merge() - first parameter has to be an object, not ' +
        typeof obj1 + '.');
    }

    if (obj2 === null || typeof obj2 !== 'object') {
      throw new TypeError(
        'merge() - first parameter has to be an object, not ' +
        typeof obj2 + '.');
    }

    if (Array.isArray(obj1) || Array.isArray(obj2)) {
      throw new TypeError('merge() - Unsupported for arrays.');
    }

    for (var k in obj2) {
      var obj1Val, obj2Val = obj2[k];
      if (Object.prototype.hasOwnProperty.call(obj1, k)) {
        if (!appendOnly) {
          obj1Val = obj1[k];
          if (obj1Val !== null && typeof obj1Val === 'object' &&
            obj2Val !== null && typeof obj2Val === 'object') {
            merge(obj1Val, obj2Val);
          } else {
            obj1[k] = clone(obj2Val);
          }
        }
      } else {
        obj1[k] = clone(obj2Val);
      }
    }
    return obj1;
  };

const checkExpiredToken =
  exports.checkExpiredToken = function checkExpiredToken(tokenHash) {
    const delta = (new Date().getTime() / 1000) - tokenHash.created_at;
    return (delta < tokenHash.expires_in) ? false : true;
  };