/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

// var meld = require('meld');
// const generate = require('./generate');
// const api = require('./api');
const dmpMessage = require('./resources/clients/Message');
// const helper = require('../../helper');
// const client = require('../../client');

/**
 * [Google description]
 * @param {[type]} messageMetadata [description]
 * @param {[type]} config          [description]
 */
function Google(messageMetadata, config) {
  let that = this;

  let data = {};
  let message = {};

  that.assistantHandle = null;
  that.requestBody = null;

  that.assistantHandler = function(assistant) {
    if (assistant == null) {
      throw new Error('You must supply the assistant to DMP.');
    }
    that.assistantHandle = assistant;

    that.assistantHandle.originalDoResponse = assistant.doResponse_;
    that.assistantHandle.doResponse_ = doResponse;

    console.log('data>>>>>', data);

    that.requestBody = assistant.body_;
    message.request = that.requestBody;
    // metadata.message_type = 'Incoming';

    data.data = message;

    console.log('data>>>>>>', data);

    dmpMessage().create(data, messageMetadata, config);
  };

  function doResponse(response, responseCode) {
    message.response = response;
    that.assistantHandle.originalDoResponse(response, responseCode);
  }
}

module.exports = Google;
