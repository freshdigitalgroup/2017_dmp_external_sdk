/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const dmpMessage = require('./resources/clients/Message');

/**
 * [Facebook description]
 * @param {[type]} metadata [description]
 * @param {[type]} options  [description]
 */
function Facebook(metadata, options) {
  const that = this;

  let data = {};
  let message = {};

  that.message = function(requestBody, responseBody, callback) {
    let requestMessageEntry = requestBody.entry[0];
    let requestMessageData = requestMessageEntry.messaging[0].message;
    let requestMessageTime = requestMessageEntry.messaging[0].timestamp;

    let responseMessageEntry = responseBody.entry[0];
    let responseMessageData = responseMessageEntry.messaging[0].message;
    let responseMessageTime = requestMessageEntry.messaging[0].timestamp;

    message.request = requestMessageData;
    message.request.timestamp = requestMessageTime;
    message.response = responseMessageData;
    message.response.timestamp = responseMessageTime;

    data.data = message;
    data.skill_user_id = requestMessageEntry.sender.id;

    const executeApi = dmpMessage(data, metadata, options, callback);
    Promise.all([executeApi]).catch(function(errors) {
      console.error(errors);
    });
  };
}
module.exports = Facebook;
