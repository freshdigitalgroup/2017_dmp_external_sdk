/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const api = require('./api');

/**
 * @param  {Object} destObject A FDG resource e.g. Invoice
 * @param  {Array} operations Rest operations that the destObject will allow
 *                            e.g. get
 * @return {Object}
 */
function mixin(destObject, operations) {
  operations.forEach(function(property) {
    destObject[property] = restFunctions[property];
  });
  return destObject;
}

const restFunctions = {
  create: function create(data, cb) {
    api.executeHttp('POST', this.baseURL, data, this.config, cb);
  },
  get: function get(id, data, cb) {
    api.executeHttp('GET', this.baseURL + id, data, this.config, cb);
  },
  list: function list(data, cb) {
    if (typeof data === 'function') {
      this.config = data;
      data = {};
    }
    api.executeHttp('GET', this.baseURL, data, this.config, cb);
  },
  del: function del(id, data, cb) {
    api.executeHttp('DELETE', this.baseURL + id, data, this.config, cb);
  },
  update: function update(id, data, cb) {
    api.executeHttp('PUT', this.baseURL + id, data, this.config, cb);
  },
  search: function search(data, cb) {
    api.executeHttp('GET', this.baseURL + 'search', data, this.config, cb);
  },
};

module.exports.mixin = mixin;
