/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const dmpMessage = require('./resources/clients/Message');

/**
 * [Cortana description]
 * @param {[type]} messageMetadata [description]
 * @param {[type]} config          [description]
 */
function Cortana(messageMetadata, config) {
  const that = this;

  let data = {};
  let message = {};

  that.receive = function(session, next) {
    console.log('incoming session>>>', session);

    message.request = session;
    console.log('incoming message>>>', message);

    next();
  };

  that.send = function(session, next) {
    console.log('outgoing session>>>', session);

    message.response = session;
    data.data = message;
    data.skill_user_id = session.address.user.id;
    console.log('total data>>>', data);

    dmpMessage(data, messageMetadata, config, function(error) {
      console.log(error);
    });

    next();
  };
}
module.exports = Cortana;
