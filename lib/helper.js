/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

exports.alexaHandler = function alexaHandler(req, outputSpeech, reprompt) {
  if (outputSpeech === '' && reprompt === '') return req;
  let res = {};
  res = req;
  if (!res.response) {
    res.response = {
      'outputSpeech': {},
      'speechletResponse': {
        'outputSpeech': {},
        'shouldEndSession': true,
      },
    };
  }
  res.response.outputSpeech = {
    'ssml': outputSpeech,
    'type': 'SSML',
  };
  res.response.speechletResponse.outputSpeech.ssml = outputSpeech;
  if (reprompt !== '') {
    res.response.reprompt = {
      'outputSpeech': {
        'ssml': reprompt,
        'type': 'SSML',
      },
    };
    res.response.speechletResponse.reprompt = {
      'outputSpeech': {
        'ssml': reprompt,
      },
    };
    res.response.speechletResponse.shouldEndSession = false;
  }
  return res;
};

exports.fbHandler = function fbHandler(req) {
  let res = {};
  res = req;
  if (!res.response) {
    res.response = {};
  }
  let message = req.message;
  res.response = message;
  return res;
};

exports.googleHandler = function googleHandler(req) {
  let res = {};
  res = req.body_;
  return res;
};

exports.cortanaHandler = function cortanaHandler(req, outputSpeech, reprompt) {
  let res = {};
  res = {
    'sessionState': req.sessionState,
    'message': req.message,
  };
  if (outputSpeech !== '') {
    res.response = {
      'outputSpeech': outputSpeech,
    };
    if (reprompt !== '') res.response.reprompt = reprompt;
  }
  return res;
};
