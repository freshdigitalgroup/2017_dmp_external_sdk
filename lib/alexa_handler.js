/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const meld = require('meld');
const dmpMessage = require('./resources/clients/Message');

/**
 * Alexa SDK for DMP
 * @param {Object} metadata Object contains Alexa metadata
 * @param {Object} options  Object contains Alexa http options
 */
function Alexa(metadata, options) {
  let data = {};
  let message = {};
  const that = this;

  const setupAspectJoinpoint = function(joinpoint) {
    try {
      const event = joinpoint.args[0];
      message = event;
      const context = joinpoint.args[1];
      const callback = joinpoint.args[2];
      const contextDone = context.done;
      context.newDone = context.done;
      context.done = function(error, result) {
        // console.log('not done yet');
      };

      setupContextAspect(context, event, callback, contextDone);
    } catch (ex) {
      console.error(ex);
    }
    joinpoint.proceed();
  };

  const setupContextAspect = function(context, event, callback, contextDone) {
    meld.around(context, 'succeed', function(joinpoint) {
      console.log('success!');

      const responseBody = joinpoint.args[0];
      message.response = responseBody.response;

      if (event.context) {
        data.skill_user_id = event.context.System.user.userId;
      } else if (event.session) {
        data.skill_user_id = event.session.System.user.userId;
      }
      data.data = message;
      data.message_type = metadata.message_type;

      joinpoint.proceed();

      dmpMessage(data, metadata, options, callback);
      context.newDone(null, responseBody);

      // const joinPoint = joinpoint.proceed();

      // Promise.all([joinPoint]).then(function() {
      //   dmpMessage(data, metadata, options, callback);
      // }).catch(function(errors, res) {
      //   console.log('error:', errors);
      //   callback(errors, res);
      // });
    });

    meld.around(context, 'fail', function(joinpoint) {
      console.log('fail!');

      const responseBody = joinpoint.args[0];
      message.response = responseBody.response;

      if (event.context) {
        data.skill_user_id = event.context.System.user.userId;
      } else if (event.session) {
        data.skill_user_id = event.session.System.user.userId;
      }

      data.data = message;
      data.message_type = 'ErrorMsg';

      joinpoint.proceed();

      dmpMessage(data, metadata, options, callback);
      context.newDone(responseBody, null);

      // const joinPoint = joinpoint.proceed();

      // Promise.all([joinPoint]).then(function() {
      //   dmpMessage(data, metadata, options, callback);
      // }).catch(function(errors, res) {
      //   console.log('error:', errors);
      //   callback(errors, res);
      // });
    });
  };

  /**
   * Alexa export handler function
   * @param  {[type]} handler [description]
   * @return {[type]}         [description]
   */
  that.handler = function(handler) {
    return meld.around(handler, setupAspectJoinpoint);
  };
}

module.exports = Alexa;
