/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

// const generate = require('../../generate');
const api = require('../../api');
// const helper = require('../../helper');

// const client = require('../../client');

/**
 * [message description]
 * @param  {[type]}   data     [description]
 * @param  {[type]}   metadata [description]
 * @param  {[type]}   options  [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
function message(data, metadata, options, callback) {
  const baseURL = '/v1/messages';
  // const operations = [];

  data.account_id = metadata.account_id;
  data.platform = metadata.platform;

  console.log('final data is', data);

  api.executeHttp('POST', baseURL, data, options, callback);
}

module.exports = message;
