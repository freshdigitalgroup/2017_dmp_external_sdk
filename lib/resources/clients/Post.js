/* Copyright 2017-2018 Fresh Digital, Inc. */
/* jshint node: true, devel: true, esversion: 6 */

'use strict';

const generate = require('../../generate');
// const api = require('../../api');

/**
 * [post description]
 * @param  {[type]} metadata [description]
 * @param  {[type]} options  [description]
 * @return {[type]}          [description]
 */
function post(metadata, options) {
  const baseURL = '/v1/clients/posts/';
  const operations = ['get', 'list', 'search'];

  let ret = {
    baseURL: baseURL,
    config: options,
  };

  ret = generate.mixin(ret, operations);
  return ret;
}

module.exports = post;
