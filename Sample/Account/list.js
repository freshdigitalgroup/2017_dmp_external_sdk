'use strict';

const DMP = require('../../');
require('../configure');

DMP.generateToken(auth, function(error, res) {
	console.log(res);
});


DMP.account.get( ['accounts'], null, function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Get Account Response');
        console.log(res);
    }
});
