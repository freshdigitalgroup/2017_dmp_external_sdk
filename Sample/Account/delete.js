'use strict';

const DMP = require('../../');
require('../configure');

const accountId = 'INV2-JSN3-NVZG-QAR3-JZQY';

DMP.account.del(accountId, function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Delete Account Response');
        console.log(res);
    }
});
