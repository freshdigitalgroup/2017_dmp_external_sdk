'use strict';

const DMP = require('../../');
require('../configure');

const createAccountJson = {
  "name": "TestAccount3", 
  "domain": "Test3", 
  "active": "true", 
  "deletable": "true" 
};

DMP.account.create(createAccountJson, ['accounts'],null, function(error, data) {
    if (error) {
        console.error(error);
    } else {
        console.log(data);
    }
});

