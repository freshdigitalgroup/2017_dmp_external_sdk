'use strict';

const DMP = require('../../');
require('../configure');

const accountId = '2';

DMP.account.get(accountId, ['accounts'], null, function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Get Account Response');
        console.log(res);
    }
});