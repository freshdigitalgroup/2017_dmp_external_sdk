'use strict';

const DMP = require('../../');
require('../configure');

const searchParams = {
  "title": "TestTitle", // title of the post
  "category_ids": "1",	// category ID
  "category_ids": "2",	// category ID
  "category_ids": "3"		// category ID
};

DMP.post.search(searchParams, null, function(error, data) {
    if (error) {
        console.error(error);
    } else {
        console.log(data);
    }
});

