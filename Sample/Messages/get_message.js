'use strict'

const DMP = require('../../');
require('../configure');

const metadata = {
  "account_id": "75e0753e-2e08-4d8d-9c7d-5a65824e828c",
  "platform": "AMZN",
  "message_type": "incoming",
}

const message_id = '1234567890'

DMP.message.get(message_id, '', metadata,function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Get Account Response');
        console.log(res);
        that.emit(':tell', speech);
    }
});