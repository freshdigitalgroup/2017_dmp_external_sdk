'use strict'

const DMP = require('../../');
require('../configure');

const metadata = {
  "account_id": "75e0753e-2e08-4d8d-9c7d-5a65824e828c",
  "platform": "AMZN",
  "message_type": "incoming",
}

const message_item = {
  "session": {
    "new": true,
    "sessionId": "SessionId.12c02a84-69cb-4019-9073-e2b455a91688",
    "application": {
      "applicationId": "amzn1.ask.skill.6c4d6c5c-eb09-4fdb-88ef-35110abfc28e"
    },
    "attributes": {},
    "user": {
      "userId": "amzn1.ask.account.AGLD2M5WJ23SAMG6OO4CE7PWHQYP6YE3PRC6IDZLQDFE2D2XZC3G3UVLGUZPR5SYBX3K23S25COJSFUAJGH5NQI7DYKAXO7KODJOTIQUQHZO3I7OZHEO5QBUABZDWYRHDHOMX64PNRDW2MKWXNXT43SQN3E2YMDJCXXY4R2UW7CYYRTGUP37OGDHZDUNDU2NQLEHFKRDNF2WNGY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.22a2cdd2-fbd0-46b8-a53e-35a117bb617f",
    "intent": {
      "name": "PlayIntent",
      "slots": {}
    },
    "locale": "en-US",
    "timestamp": "2017-08-18T01:19:04Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.6c4d6c5c-eb09-4fdb-88ef-35110abfc28e"
      },
      "user": {
        "userId": "amzn1.ask.account.AGLD2M5WJ23SAMG6OO4CE7PWHQYP6YE3PRC6IDZLQDFE2D2XZC3G3UVLGUZPR5SYBX3K23S25COJSFUAJGH5NQI7DYKAXO7KODJOTIQUQHZO3I7OZHEO5QBUABZDWYRHDHOMX64PNRDW2MKWXNXT43SQN3E2YMDJCXXY4R2UW7CYYRTGUP37OGDHZDUNDU2NQLEHFKRDNF2WNGY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

const speech = "<audio src='https://d12y3e7lbn1vso.cloudfront.net/assets/audio/National_Anthem_La_Marseillaise.mp3' />"

DMP.message.create(message_item, speech, metadata, function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Get Account Response');
        console.log(res);
        that.emit(':tell', speech);
    }
});

// Message with card
const card = {
  cardTitle: 'card_title',
  cardContent: 'card_content',
  cardImage: {
    smallImageURL: 'card_small_image_url',
    largeImageURL: 'card_large_image_url',
  },
};

DMP.message.create(message_item, speech, card, metadata, function(error, res) {
    if (error) {
        console.log(error.response);
        throw error;
    } else {
        console.log('Get Account Response');
        console.log(res);
        // change :tellWithCard to :askWithCard for a question
        that.emit(':tellWithCard', speech, card.cardTitle, card.cardContent, card.cardImage);
    }
});