'use strict';

const DMP = require('../../');
require('../configure');

const searchParams = {
  "name": "TestName"	// name of the category
};

DMP.category.search(searchParams, null, function(error, data) {
    if (error) {
        console.error(error);
    } else {
        console.log(data);
    }
});

